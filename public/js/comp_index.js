document.addEventListener('DOMContentLoaded', init, false);

function init() {
    
    //Burger
    
    let burger=document.getElementById("burger");
    
    burger.addEventListener("click",
                            function(){
        burger.classList.toggle("burgerchecked");
        
        if (document.getElementsByTagName("nav").item(0).style.right !== "12px")
            document.getElementsByTagName("nav").item(0).style.right="12px";
        else 
            document.getElementsByTagName("nav").item(0).style.right="-202px";
    }
                           );
    
    //Logo Canvas
    
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    
        //Fleche
    
    context.beginPath();
    context.moveTo(50,0);
    context.lineTo(35,20);
    context.lineTo(45,20);
    context.lineTo(45,50);
    context.lineTo(55,50);
    context.lineTo(55,20);
    context.lineTo(65,20);
    context.lineTo(50,0);
    context.closePath();
    
    context.fillStyle="#27ae60";
    context.fill();
    
        //Boite
    
    context.beginPath();
    context.moveTo(0,60);
    context.lineTo(100,60);
    context.lineTo(100,140);
    context.lineTo(0,140);
    context.lineTo(0,60);
    context.closePath();
    
    context.fillStyle="#925a24";
    context.fill();
    
        //Scotch
    
    context.beginPath();
    context.moveTo(40,60);
    context.lineTo(60,60);
    context.lineTo(60,80);
    context.lineTo(55,75);
    context.lineTo(50,80);
    context.lineTo(45,75);
    context.lineTo(40,80);
    context.lineTo(40,60);
    context.closePath();
    
    context.fillStyle="#503215";
    context.fill();
        
        //Etiquette 1
    
    context.beginPath();
    context.moveTo(10,100);
    context.lineTo(35,100);
    context.lineTo(35,110);
    context.lineTo(10,110);
    context.lineTo(10,100);
    context.closePath();
    
    context.fillStyle="#ededed";
    context.fill();
    
        //Etiquette 2
    
    context.beginPath();
    context.moveTo(10,120);
    context.lineTo(50,120);
    context.lineTo(50,130);
    context.lineTo(10,130);
    context.lineTo(10,120);
    context.closePath();
    
    context.fillStyle="#ededed";
    context.fill();

}